== About these patches ==

As of November 1st, 2010, the two patches against Simple Payments included in this directory are required for views integration in the 2.x branch of Simple Donations.

We are working with the maintainer of the Simple Payments module to get these patches accepted.